const { join, resolve } = require('path');
const { createReadStream } = require('fs');

const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');

module.exports = {
    context: __dirname,
    entry: join( __dirname, 'src/main.js' ),
    output: join( __dirname, 'public/bundle.js' ),

    resolve: {
        extensions: ['.js', '.vue', '.json'],
        modules: [
            resolve('./src/'),
            resolve('./node_modules'),
        ],
    },

    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader',
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            { 
                test: /\.css$/, 
                use: [ 'style-loader', 'css-loader' ]
            }
        ],
    },

    plugins: [
        new HtmlPlugin({
            filename: 'index.html',
            template: 'index.html',
            inject: true,
            mobile: true,
            appMountId: 'app'
        }),

        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: `"${process.env.NODE_ENV}"`,
            },
        }),
    ],

    devServer: {
        historyApiFallback: true,
        contentBase: './public/',
        hot: true,
        port: 9000,
        setup(app) {
            app.get('/api/user', function(req, res)  {
                res.writeHead(200, { 'Content-Type' : 'application/json' });
                createReadStream(join(process.cwd(), 'api/user.json'), { encoding: 'utf-8' })
                    .pipe(res);
            });
        },
    },
};
