import Vue from 'vue'
import Router from 'vue-router'
import lazyLoading from './lazyLoading'

Vue.use(Router);

const routes = [
  { 
    name: 'home', 
    path: '/home', 
    component: lazyLoading('home/Home'),
    meta: {
      title: 'Home'
    }
  },
  { 
    name: 'orders', 
    path: '/orders/:orderId?', 
    component: lazyLoading('orders/Orders'),
    meta: {
      title: 'My orders'
    }
  },
  { 
    path: '*',
    redirect: {
      name: 'home'
    }
  },  
];

const router = new Router({
  routes
}); 

router.beforeEach((to, from, next) => {
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  if (nearestWithTitle) {
    document.title = nearestWithTitle.meta.title;
  }

  next();
});

export default router;
