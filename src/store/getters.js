const getUser = state => state.app.user
const getOrders = state => state.app.orders

export default {
  getUser,
  getOrders
}
