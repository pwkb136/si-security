import * as types from '../mutation-types'

const state = {
  user: {},
  orders: []
}

const mutations = {
  [types.SET_USER] (state, user) {
    state.user = user;
  },
  [types.ADD_ORDER] (state, order) {
    order.id = state.orders.length + 1;
    order.canceled = false;

    state.orders.push(order);
  },  
  [types.BUY] (state, price) {
    state.user.usd -= price;
  },  
  [types.SELL] (state, price) {
    state.user.btc -= price;
  },
  [types.CANCEL_ORDER] (state, index) {
    let order = state.orders[index];
    console.log(order);
    order.canceled = true;

    if (order.type === 'buy') {
      state.user.usd += order.price;
    } else {
      state.user.btc += order.price;
    }    
  }
}

const actions = {
  setUser ({ commit }, user) {
    commit(types.SET_USER, user)
  },
  addOrder ({ commit }, order) {
    commit(types.ADD_ORDER, order)
  },
  buy ({ commit }, price) {
    commit(types.BUY, price)
  },
  sell ({ commit }, price) {
    commit(types.SELL, price)
  },
  cancelOrder({ commit }, index) {
    commit(types.CANCEL_ORDER, index)
  }
}

export default {
  state,
  mutations,
  actions
}
