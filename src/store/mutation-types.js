export const SET_USER = 'SET_USER'
export const ADD_ORDER = 'ADD_ORDER'

export const BUY = 'BUY'
export const SELL = 'SELL'

export const CANCEL_ORDER = 'CANCEL_ORDER'
